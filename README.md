Solution for task 7

After running the program, 25 different animals will be generated. These animals will be different subtypes of animal, and all implement a Act() method. The bird class was added to demonstrate abstract classes, and it is inherited by both the GlidingBird and FlappyBird class. 

Once all animals are created, Act() will be called on all of them. This demonstrates that they behave differently for the same function.

The relevant code is found in /task7/Program.cs, and all animal classes are placed in /task7/Animals/
