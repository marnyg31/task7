﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task7
{
    class Pig : Animal
    {
        public Pig(string name, double heightInMeters, int numLegs) : base(name, heightInMeters, numLegs)
        {
            //User base constructor
        }
        public Pig(string name, int numLegs) : base(name, numLegs)
        {
            //User base constructor
        }

        public override void Act()
        {
            //Print the action of the animal type
            Console.WriteLine(this.Name + " is does pig stuff with " + this.NumLegs + " legs");
        }
    }
}
