﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task7
{
    class Animal
    {
        //Defining props
        public string Name { get; set; }
        public double HeightInMeters { get; set; }
        public int NumLegs { get; set; }

        public Animal(string name, double heightInMeters, int numLegs)
        {
            //Defining constructor
            this.Name = name;
            this.HeightInMeters = heightInMeters;
            this.NumLegs = numLegs;
        }

        public Animal(string name, int numLegs)
        {
            //Overloading constructor
            this.Name = name;
            this.HeightInMeters = 0;
            this.NumLegs = numLegs;
        }

        public virtual void Act()
        {
            //Act method shared by all animals, unless overridden
            Console.WriteLine(Name + " is beeing random and qirky, with " + this.NumLegs + " legs");
        }

    }
}