﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task7
{
    class FlyingBird:Bird
    {
        public FlyingBird(string name, double heightInMeters, int numLegs) : base(name, heightInMeters, numLegs)
        {
            //User base constructor
        }
        public FlyingBird(string name, int numLegs) : base(name, numLegs)
        {
            //User base constructor
        }

        public override void fly()
        {
            //Print the action of the animal type
            Console.WriteLine(this.Name+" is flapping it's wings, with "+this.NumLegs+" legs");
        }
    }
}
