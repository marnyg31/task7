﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Task7
{
    class Program
    {
        static void Main(string[] args)
        {
            //Creat alist of different animals subtypes and call Act() on all of them
            List<Animal> animals = new List<Animal>();
            for (int i = 0; i < 25; i++) animals.Add(CreateAnimalBasedOnIndex(i));
            foreach (Animal animal in animals) animal.Act();
        }

        private static Animal CreateAnimalBasedOnIndex(int index)
        {
            //Creat different animals subtypes based on index of for loop 
            Random random = new Random();
            if (index < 5) return (new Animal("animal" + index, random.NextDouble(), index % 4));
            else if (index < 10) return (new GlidingBird("glider" + index, random.NextDouble(), 2));
            else if (index < 15) return (new FlyingBird("flappy" + index, random.NextDouble(), 2));
            else if (index < 20) return (new Pig("pig" + index, random.NextDouble(), 4));
            else return (new Snake("snek" + index, random.NextDouble(), 0));
        }
    }
}
